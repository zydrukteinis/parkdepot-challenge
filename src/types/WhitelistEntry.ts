export type WhitelistEntry = {
	id?: string;
	licensePlate: string;
	createdBy?: string;
	infiniteValidity: boolean;
	validFrom: Date | string;
	validTo: Date | string;
	updatedAt?: string;
	updatedBy?: string;
};

export type WhitelistEntryErrors = {
	licensePlate: boolean;
	validFrom: boolean;
	validTo: boolean;
};