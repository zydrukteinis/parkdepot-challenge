export type Column = {
    title: string; field: string; type?: any; lookup?: any, render?: ((props: any) => React.ReactElement<any>),
};