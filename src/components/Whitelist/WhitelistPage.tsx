import React from 'react';
import { Column } from '../../types/Column';
import { WhitelistEntry } from '../../types/WhitelistEntry';
import MaterialTable from 'material-table';
import Checkbox from '@material-ui/core/Checkbox';
import CircularProgress from '@material-ui/core/CircularProgress';

type Props = {
    userRole: string;
    userEmail: string;
	whitelist: WhitelistEntry[];
    loading: boolean;
    deleteWhitelistEntry(id: string | undefined): void;
    openWhitelistEntryEdit(rowData: any): void;
};

export default function WhitelistPage({
    userRole,
    userEmail,
    whitelist,
    loading,
    deleteWhitelistEntry,
    openWhitelistEntryEdit
}: Props) {
    const isEditDeleteAllowed = (whitelistEntry: WhitelistEntry) => {
        if (userRole === 'Manager') {
            return true;
        }

        return whitelistEntry.createdBy === userEmail;
    };

	const columns: Column[] = [
		{
			title: 'License Plate',
			field: 'licensePlate',
        },
		{
			title: 'Valid From',
			field: 'validFrom',
		},
		{
			title: 'Valid To',
			field: 'validTo',
		},
		{
			title: 'Infinite Duration',
			field: 'infiniteValidity',
			type: 'boolean',
			render: rowData => <Checkbox
				checked={rowData.infiniteValidity}
				inputProps={{ 'aria-label': 'primary checkbox' }}
			/>
		},
	];

	return (
		<React.Fragment>
			<MaterialTable
                title={'Whitelist'}
                columns={columns}
                data={whitelist}
                editable={{
                    isDeleteHidden: (whitelistEntry: WhitelistEntry) => !isEditDeleteAllowed(whitelistEntry),
                    isEditHidden: (whitelistEntry: WhitelistEntry) => !isEditDeleteAllowed(whitelistEntry),
                    isEditable: (whitelistEntry: WhitelistEntry) => !isEditDeleteAllowed(whitelistEntry),
                    onRowDelete: (rawData) =>
                        new Promise((resolve) => {
                            setTimeout(() => {
                            resolve(rawData);
                            deleteWhitelistEntry(rawData.id)
                            }, 600);
                        }),
                }}
                localization={{
                    header: {
                        actions: 'Actions'
                    },
                    body: {
                        editRow: {
                            deleteText: 'Are you sure delete this whitelist entry?',
                            cancelTooltip: 'Cancel',
                            saveTooltip: 'Confirm',
                        },
                        emptyDataSourceMessage: loading
                            ? <CircularProgress size={50}/>
                            : 'Whitelist is currently empty',
                    },
                    pagination: {
                        labelRowsSelect: 'rows',
                        firstAriaLabel: 'First Page',
                        firstTooltip: 'First Page',
                        previousAriaLabel: 'Previous Page',
                        previousTooltip: 'Previous Page',
                        nextAriaLabel: 'Next Page',
                        nextTooltip: 'Next Page',
                        lastAriaLabel: 'Last Page',
                        lastTooltip: 'Last Page',
                    },
                    toolbar: {
                        searchTooltip: 'Search',
                        searchPlaceholder: 'Search',
                    },
                }}
                actions={[(whitelistEntry: WhitelistEntry) => ({
                    icon: 'edit',
                    tooltip: 'Edit',
                    onClick: (event, rowData) => openWhitelistEntryEdit(rowData),
                    hidden: !isEditDeleteAllowed(whitelistEntry)
                })]}
                options={{
                    actionsColumnIndex: -1,
                }}
            />
		</React.Fragment>
	);
};