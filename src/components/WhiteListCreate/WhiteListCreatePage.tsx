import React from 'react';
import { WhitelistEntry, WhitelistEntryErrors } from "../../types/WhitelistEntry";
import { makeStyles } from '@material-ui/core/styles';
import Container from '@material-ui/core/Container';
import Paper from '@material-ui/core/Paper';
import Typography from '@material-ui/core/Typography';
import Button from '@material-ui/core/Button';
import WhiteListForm from '../WhiteListForm/WhiteListForm';

type Props = {
	loading: boolean;
	whitelistEntry: WhitelistEntry;
	whitelistEntryErrors: WhitelistEntryErrors;
	addToWhitelist(): void;
	handleChange(event: React.ChangeEvent<{ name: string, value: any, valueAsNumber?: number, }>): void;
	handleToggle(event: React.ChangeEvent<HTMLInputElement>): void;
};

const useStyles = makeStyles((theme) => ({
	paper: {
		marginTop: theme.spacing(3),
		marginBottom: theme.spacing(3),
		padding: theme.spacing(2),
		[theme.breakpoints.up(600 + theme.spacing(3) * 2)]: {
			marginTop: theme.spacing(6),
			marginBottom: theme.spacing(6),
			padding: theme.spacing(3),
		},
	},
	buttons: {
		display: 'flex',
		justifyContent: 'flex-end',
	},
	button: {
		marginTop: theme.spacing(3),
		marginLeft: theme.spacing(1),
	},
	progress: {
		marginTop: theme.spacing(2),
	},
}));

export default function WhitelistCreatePage({
	loading,
	whitelistEntry,
	whitelistEntryErrors,
	addToWhitelist,
	handleChange,
	handleToggle
}: Props) {
	const classes = useStyles();

	return (
		<React.Fragment>
			<Container maxWidth="md">
				<Paper className={classes.paper}>
					<Typography component="h1" variant="h4" align="center">
						Add Car To Whitelist
					</Typography>
					<WhiteListForm
						whitelistEntry={whitelistEntry}
						whitelistEntryErrors={whitelistEntryErrors}
						handleChange={handleChange}
						handleToggle={handleToggle}
					/>
					<div className={classes.buttons}>
						<Button disabled={loading} className={classes.button}
							variant="contained"
							color="primary"
							onClick={addToWhitelist}
						>
							Submit
						</Button>
					</div>
				</Paper>
			</Container>
		</React.Fragment>
	);
}