import React from 'react';
import { WhitelistEntry, WhitelistEntryErrors } from "../../types/WhitelistEntry";
import { makeStyles } from '@material-ui/core/styles';
import Grid from '@material-ui/core/Grid';
import TextField from '@material-ui/core/TextField';
import FormControl from '@material-ui/core/FormControl';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import Switch from '@material-ui/core/Switch'

type Props = {
	whitelistEntry: WhitelistEntry;
	whitelistEntryErrors: WhitelistEntryErrors;
	handleChange(event: React.ChangeEvent<{ name: string, value: any, valueAsNumber?: number, }>): void;
	handleToggle(event: React.ChangeEvent<HTMLInputElement>): void;
};

const useStyles = makeStyles((theme) => ({
	form: {
		paddingTop: theme.spacing(6),
		paddingBottom: theme.spacing(2),
	},
	buttons: {
		display: 'flex',
		justifyContent: 'flex-end',
	},
	button: {
		marginTop: theme.spacing(3),
		marginLeft: theme.spacing(1),
	},
	textField: {
		marginLeft: theme.spacing(1),
		marginRight: theme.spacing(1),
	},
}));

export default function WhiteListForm({
	whitelistEntry,
	whitelistEntryErrors,
	handleChange,
	handleToggle,
}: Props) {
	const classes = useStyles();

	const getLicensePlateHelperText = () => {
		return {
			...(whitelistEntryErrors.licensePlate && {
				helperText: 'License plate cannot be empty'
			}),
		};
	};

	const getDatetimeRangeHelperText = () => {
		return {
			...(!whitelistEntry.infiniteValidity
					&& whitelistEntryErrors.validFrom
					&& whitelistEntryErrors.validTo
					&& {
				helperText: 'Incorrect date & time range'
			}),
		};
	};

	return (
		<form className={classes.form} noValidate autoComplete="off">
			<Grid container spacing={3}>
				<Grid item xs={6} sm={6} lg={6}>
					<FormControl error={whitelistEntryErrors.licensePlate} fullWidth>
						<TextField
							error={whitelistEntryErrors.licensePlate}
							name="licensePlate"
							id="licensePlate"
							label={'License Plate'}
							className={classes.textField}
							value={whitelistEntry.licensePlate}
							onChange={handleChange}
							{...getLicensePlateHelperText()}
						/>
					</FormControl>
				</Grid>
				<Grid item xs={6} sm={6} lg={6}>
				</Grid>
				<Grid item xs={12} sm={6} lg={6}>
					<TextField
						name="validFrom"
						id="validFrom"
						label="Valid From"
						type="datetime-local"
						className={classes.textField}
						value={whitelistEntry.validFrom}
						onChange={handleChange}
						InputLabelProps={{
							shrink: true,
						}}
						disabled={whitelistEntry.infiniteValidity}
						error={!whitelistEntry.infiniteValidity && whitelistEntryErrors.validFrom}
						{...getDatetimeRangeHelperText()}
					/>
				</Grid>
				<Grid item xs={12} sm={6} lg={6}>
					<TextField
						name="validTo"
						id="validTo"
						label="Valid To"
						type="datetime-local"
						className={classes.textField}
						value={whitelistEntry.validTo}
						onChange={handleChange}
						InputLabelProps={{
							shrink: true,
						}}
						disabled={whitelistEntry.infiniteValidity}
						error={!whitelistEntry.infiniteValidity && whitelistEntryErrors.validTo}
						{...getDatetimeRangeHelperText()}
					/>
				</Grid>
				<Grid item xs={12} sm={12} lg={12}>
					<FormControl fullWidth>
						<FormControlLabel
							control={<Switch checked={whitelistEntry.infiniteValidity} onChange={handleToggle} name="infiniteValidity" />}
							label="Infinite Duration"
						/>
					</FormControl>
				</Grid>
			</Grid>
		</form>
	);
};