import React from 'react';
import clsx from 'clsx';
import { getUserRole } from '../../helpers/role';
import { NavLink } from 'react-router-dom'
import { makeStyles, useTheme } from '@material-ui/core/styles';
import Drawer from '@material-ui/core/Drawer';
import IconButton from '@material-ui/core/IconButton';
import ChevronLeftIcon from '@material-ui/icons/ChevronLeft';
import ChevronRightIcon from '@material-ui/icons/ChevronRight';
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import ListItemIcon from '@material-ui/core/ListItemIcon';
import ListItemText from '@material-ui/core/ListItemText';
import Divider from '@material-ui/core/Divider';
import AddBoxIcon from '@material-ui/icons/AddBox'
import ViewListIcon from '@material-ui/icons/ViewList';

type Props = {
	user: any;
	open: boolean;
	setOpen(isOpen: boolean): void;
};

const useStyles = makeStyles((theme) => ({
	hide: {
		display: 'none',
	},
	drawer: {
		width: 240,
		flexShrink: 0,
		whiteSpace: 'nowrap',
	},
	drawerOpen: {
		width: 240,
		transition: theme.transitions.create('width', {
			easing: theme.transitions.easing.sharp,
			duration: theme.transitions.duration.enteringScreen,
		}),
	},
	drawerClose: {
		transition: theme.transitions.create('width', {
			easing: theme.transitions.easing.sharp,
			duration: theme.transitions.duration.leavingScreen,
		}),
		overflowX: 'hidden',
		width: theme.spacing(7) + 1,
		[theme.breakpoints.up('sm')]: {
			width: theme.spacing(9) + 1,
		},
	},
	toolbar: {
		display: 'flex',
		alignItems: 'center',
		justifyContent: 'flex-end',
		padding: theme.spacing(0, 1),
		...theme.mixins.toolbar,
	},
}));

export default function SideMenu({ user, open, setOpen } : Props) {
	const theme = useTheme();
	const classes = useStyles();
	const handleDrawerClose = () => {
		setOpen(false);
	};

	return (
		<Drawer
			variant="permanent"
			className={clsx(classes.drawer, {
					[classes.drawerOpen]: open,
					[classes.drawerClose]: !open,
				})
			}
			classes={{
				paper: clsx({
					[classes.drawerOpen]: open,
					[classes.drawerClose]: !open,
				}),
			}}
		>
			<div className={classes.toolbar}>
			<IconButton onClick={handleDrawerClose}>
				{theme.direction === 'rtl' ? <ChevronRightIcon /> : <ChevronLeftIcon />}
			</IconButton>
			</div>
			<Divider />
			<List>
				<ListItem component={NavLink} to="/whitelist" button key={'whitelist'}>
					<ListItemIcon> <ViewListIcon /></ListItemIcon>
					<ListItemText primary={'Whitelist'} />
				</ListItem>
				{ getUserRole(user) &&
					<ListItem component={NavLink} to="/whitelist/add" button key={'newProduct'}>
						<ListItemIcon> <AddBoxIcon /></ListItemIcon>
						<ListItemText primary={'Add to the Whitelist'} />
					</ListItem>
				}
			</List>
		</Drawer>
	)
}