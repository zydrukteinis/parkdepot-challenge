import React from 'react';
import clsx from 'clsx';
import { useAuth0 } from "@auth0/auth0-react";
import { makeStyles } from '@material-ui/core/styles';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import Typography from '@material-ui/core/Typography';
import IconButton from '@material-ui/core/IconButton';
import MenuIcon from '@material-ui/icons/Menu';
import UserMenu from '../UserMenu/UserMenu';
import SideMenu from './SideMenu';
import logo from '../../Assets/logo-light.png';

const useStyles = makeStyles((theme) => ({
	root: {
		flexGrow: 1,
	},
	menuButton: {
		marginRight: theme.spacing(2),
	},
	title: {
		flexGrow: 1,
		textAlign: 'left',
	},
	appBar: {
		zIndex: theme.zIndex.drawer + 1,
		transition: theme.transitions.create(['width', 'margin'], {
			easing: theme.transitions.easing.sharp,
			duration: theme.transitions.duration.leavingScreen,
		}),
	},
	logo: {
		marginTop: theme.spacing(1),
	},
}));


export default function Navigation() {
	const [ open, setOpen ] = React.useState(false);
	const { user, isAuthenticated, isLoading, logout } = useAuth0();
	const classes = useStyles();

	const getUserMenu = () => {
		if (isLoading) {
			return (
				<>
				</>
			);
		}
		return !isAuthenticated ? (
			<></>
		) : (
			<UserMenu user={user} logout={logout}/>
		);
	};

	return (
		<React.Fragment>
			<AppBar position="fixed"
				className={clsx(classes.appBar)}>
				<Toolbar>
					<IconButton
						edge="start"
						className={classes.menuButton}
						color="inherit"
						aria-label="menu"
						onClick={() => {setOpen(!open) }}
					>
						{isAuthenticated && <MenuIcon /> }
					</IconButton>
					<Typography variant="h6" className={classes.title}>
						<img alt="nav-logo" className={classes.logo} src={logo}/>
					</Typography>
					{ getUserMenu() }
				</Toolbar>
			</AppBar>
			{
				isAuthenticated && <SideMenu user={user} open={open} setOpen={setOpen}/>
			}
		</React.Fragment>
	);
}