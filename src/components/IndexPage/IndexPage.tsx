import React from 'react';
import Button from '@material-ui/core/Button';
import Grid from '@material-ui/core/Grid';
import Typography from '@material-ui/core/Typography';
import { makeStyles } from '@material-ui/core/styles';
import Container from '@material-ui/core/Container';
import logo from '../../Assets/logo-dark.png';

type Props = {
	history: any;
	isLoading: boolean;
	isAuthenticated: boolean;
	loginWithRedirect(): void;
};

export default function IndexPage({
	history,
	isLoading,
	isAuthenticated,
	loginWithRedirect
}: Props) {
	const useStyles = makeStyles((theme) => ({
		icon: {
		  marginRight: theme.spacing(2),
		},
		heroButtons: {
		  marginTop: theme.spacing(4),
		},
		container: {
			marginTop: theme.spacing(20),
		},
	}));

	const goToWhitelist = () => {
		history.replace("");
		history.push("/whitelist");
	};

	const classes = useStyles();

	return (
		<Container className={classes.container} maxWidth="sm">
			<Typography variant="h5" align="center" color="textSecondary" paragraph>
				<img alt="header-logo" src={logo}/>
			</Typography>
			<Typography variant="h6" align="center" color="textSecondary" paragraph>
				The parking lot as a digital hub.
			</Typography>
			<div className={classes.heroButtons}>
				<Grid container spacing={2} justify="center">
					<Grid item>
					{ !isLoading && !isAuthenticated &&
						<Button onClick={loginWithRedirect} variant="contained" color="primary">
							Authenticate
						</Button>
					}
					{ !isLoading && isAuthenticated &&
						<Button onClick={goToWhitelist} variant="contained" color="primary">
							Go To Whitelist
						</Button>
					}
					</Grid>
				</Grid>
			</div>
		</Container>
	);
};