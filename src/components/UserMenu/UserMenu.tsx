import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import IconButton from '@material-ui/core/IconButton';
import MenuItem from '@material-ui/core/MenuItem';
import Menu from '@material-ui/core/Menu';
import AccountCircle from '@material-ui/icons/AccountCircle';
import Divider from '@material-ui/core/Divider';
import Avatar from '@material-ui/core/Avatar';

type Props = {
	user: {
		name: string;
		picture: string;
	};
	logout(): void;
};

export default function UserMenu({ user, logout } : Props) {
	const [ anchorEl, setAnchorEl ] = React.useState<null | HTMLElement>(null);

	const handleProfileMenuOpen = (event: React.MouseEvent<HTMLElement>) => {
		setAnchorEl(event.currentTarget);
	};

	const useStyles = makeStyles((theme) => ({
		userContent: {
			display: 'flex',
			justifyContent: 'center',
			opacity: 'inherit !important',
		},
		menuItemContent: {
			justifyContent: 'center',
			opacity: 'inherit !important',
		}
	}));

	const classes = useStyles();
	const isMenuOpen = Boolean(anchorEl);
	const menuId = 'primary-search-account-menu';

	const handleMenuClose = () => {
		setAnchorEl(null);
	};

	return (
		<React.Fragment>
			<div>
				{user.name}
				<IconButton
					edge="end"
					aria-label="account of current user"
					aria-controls={menuId}
					aria-haspopup="true"
					onClick={handleProfileMenuOpen}
					color="inherit"
				>
					<AccountCircle />
				</IconButton>
			</div>
			<Menu
				anchorEl={anchorEl}
				anchorOrigin={{ vertical: 'top', horizontal: 'right' }}
				id={menuId}
				keepMounted
				transformOrigin={{ vertical: 'top', horizontal: 'right' }}
				open={isMenuOpen}
				onClose={handleMenuClose}
			>
				<MenuItem
					className={classes.userContent}
					disabled={true}
				>
					<Avatar alt={user.name} src={user.picture} />
				</MenuItem>
				<MenuItem
					className={classes.menuItemContent}
					disabled={true}
				>
					{user && user.name}
				</MenuItem>
				<Divider/>
				<MenuItem
					className={classes.menuItemContent}
					onClick={() => logout()}
				>
					Logout
				</MenuItem>
			</Menu>
		</React.Fragment>
	)
}