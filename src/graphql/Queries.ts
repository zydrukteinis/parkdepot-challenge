import { gql } from "@apollo/client";

export const ADD_CAR_TO_WHITELIST = gql`
	mutation(
		$licensePlate: String,
		$infiniteValidity: Boolean,
		$createdBy: String,
		$validFrom: timestamp,
		$validTo: timestamp
	) {
		insert_Whitelist_one(
			object: {
				licensePlate: $licensePlate,
				infiniteValidity: $infiniteValidity,
				createdBy: $createdBy,
				validFrom: $validFrom,
				validTo: $validTo
			}
		) {
			createdBy
			id
			infiniteValidity
			licensePlate
			updatedAt
			updatedBy
			validFrom
			validTo
		}
	}
`;

export const GET_WHITELIST = gql`
	query {
		Whitelist {
			createdBy
			id
			infiniteValidity
			licensePlate
			updatedAt
			validFrom
			updatedBy
			validTo
		}
	}
`;

export const DELETE_WHITELIST_ENTRY = gql`
	mutation($id: uuid!) {
		delete_Whitelist_by_pk(id: $id) {
			id
		}
	}
`;

export const UPDATE_WHITELIST_ENTRY = gql`
	mutation($id: uuid!, $licensePlate: String, $infiniteValidity: Boolean, $validFrom: timestamp, $validTo: timestamp, $updatedBy: String) {
		update_Whitelist_by_pk(
			pk_columns: { id: $id },
			_set: { licensePlate: $licensePlate, infiniteValidity: $infiniteValidity, validFrom: $validFrom, validTo: $validTo, updatedBy: $updatedBy  }
		) {
			id,
			licensePlate,
			infiniteValidity,
			validFrom,
			validTo,
			updatedBy
		}
	}
`;