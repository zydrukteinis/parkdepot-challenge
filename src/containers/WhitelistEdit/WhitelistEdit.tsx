import React, { useReducer, useState, useEffect } from 'react';
import { useAuth0 } from '@auth0/auth0-react';
import { WhitelistEntry, WhitelistEntryErrors } from '../../types/WhitelistEntry';
import { useSnackbar } from 'notistack';
import { isWhitelistEntryPropValid } from '../../helpers/validation';
import { UPDATE_WHITELIST_ENTRY, GET_WHITELIST } from '../../graphql/Queries';
import WhitelistEditPage from '../../components/WhitelistEdit/WhitelistEditPage';
import { useMutation, useQuery } from "@apollo/client";

type Props = {
	id: string;
};

type State = {
	whitelistEntry: WhitelistEntry | null;
	whitelistEntryErrors: WhitelistEntryErrors;
};

type Action =
	| { type: 'EDIT_WHITELIST_ENTRY', payload: { prop: string, value: any, } }
	| { type: 'SET_WHITELIST_PROP_ERROR', payload: { errorsProp: string, value: boolean, } }
	| { type: 'SET_WHITELIST_ENTRY', payload: { whitelistEntry: WhitelistEntry } };

const initialState: State | any = {
	whitelistEntry: null,
	whitelistEntryErrors: {
		licensePlate: false,
		validFrom: false,
		validTo: false,
	},
};

const reducer = (state = initialState, action: Action) => {
	switch (action.type) {
		case 'EDIT_WHITELIST_ENTRY':
			return {
				...state,
				whitelistEntry: {
					...state.whitelistEntry,
					[action.payload.prop]: action.payload.value
				},
			};
		case 'SET_WHITELIST_PROP_ERROR':
			return {
				...state,
				whitelistEntryErrors: {
					...state.whitelistEntryErrors,
					[action.payload.errorsProp]: action.payload.value
				},
			};
		case 'SET_WHITELIST_ENTRY':
			const whitelistEntry = action.payload.whitelistEntry;
			return {
				...state,
				whitelistEntry: {
					id: whitelistEntry.id,
					licensePlate: whitelistEntry.licensePlate,
					infiniteValidity: whitelistEntry.infiniteValidity,
					validFrom: whitelistEntry.validFrom,
					validTo: whitelistEntry.validTo,
				},
			};
		default:
			return state;
	};
};


export default function WhitelistEdit({ id } : Props) {
	const [ updateLoad, setUpdateLoad ] = useState(false);
	const { loading, data } = useQuery(GET_WHITELIST);
	const { user } = useAuth0();
	const [ state, dispatch ] = useReducer(reducer, initialState);
	const { enqueueSnackbar } = useSnackbar();
	const [ updateWhitelistEntryMutation ] = useMutation(UPDATE_WHITELIST_ENTRY);

	useEffect(() => {
		if (data && !state.whitelistEntry) {
			dispatch({
				type: 'SET_WHITELIST_ENTRY',
				payload: {
					whitelistEntry: data.Whitelist
						.find((whiteListEntryA: WhitelistEntry) => whiteListEntryA.id === id),
				}
			});
		}
	// eslint-disable-next-line react-hooks/exhaustive-deps
	}, [loading]);

	const validatePropChange = (prop: string, value: any) => {
		let isValid = isWhitelistEntryPropValid(prop, value, state.whitelistEntry);

		if (state.whitelistEntryErrors[prop] === !isValid) {
			return;
		}

		if (['validFrom', 'validTo'].includes(prop)) {
			dispatch({
				type: 'SET_WHITELIST_PROP_ERROR',
				payload: {
					errorsProp: 'validFrom',
					value: !isValid,
				}
			});
			dispatch({
				type: 'SET_WHITELIST_PROP_ERROR',
				payload: {
					errorsProp: 'validTo',
					value: !isValid,
				}
			});
			return;
		}

		dispatch({
			type: 'SET_WHITELIST_PROP_ERROR',
			payload: {
				errorsProp: prop,
				value: !isValid,
			}
		});
	};

	const handleChange = (event: React.ChangeEvent<{ name: string, value: any, valueAsNumber?: number, }>) => {
		const { name, value } = event.target;

		dispatch({
			type: 'EDIT_WHITELIST_ENTRY',
			payload: {
				prop: name,
				value,
			}
		});

		validatePropChange(name, value);
	};

	const handleToggle = (event: React.ChangeEvent<HTMLInputElement>): void => {
		const { name, checked } = event.target;

		dispatch({
			type: 'EDIT_WHITELIST_ENTRY',
			payload: {
				prop: name,
				value: checked,
			},
		});
	};

	const updateWhitelistEntry = () => {
		setUpdateLoad(true);

		const whiteListEntryErrors = Object.keys(state.whitelistEntryErrors).map((propKey) => {
			let isValid = isWhitelistEntryPropValid(
				propKey,
				state.whitelistEntry[propKey],
				state.whitelistEntry
			);

			if (state.whitelistEntryErrors[propKey] === !isValid) {
				return !isValid;
			}

			dispatch({
				type: 'SET_WHITELIST_PROP_ERROR',
				payload: {
					errorsProp: propKey,
					value: !isValid,
				}
			});

			return !isValid;
		});

		if (whiteListEntryErrors.some(isError => isError)) {
			enqueueSnackbar('Invalid Fields', { variant: 'error' });
			setUpdateLoad(false);
			return;
		}

		try {
			updateWhitelistEntryMutation({
				variables: {
					...state.whitelistEntry,
					updatedBy: user.email,
				},
				optimisticResponse: true,
				update: (cache, { data }) => {
					const currentWhitelist: { Whitelist: WhitelistEntry[] } | null = cache.readQuery({
						query: GET_WHITELIST,
					});

					if (!currentWhitelist) {
						return;
					}

					const updatedWhitelistEntry = currentWhitelist.Whitelist
						.map(whitelistEntry => whitelistEntry.id === state.whitelistEntry.id ? {
							...data.updated_Whitelist_one
						} : whitelistEntry);

					cache.writeQuery({
						query: GET_WHITELIST,
						data: { Whitelist: [...currentWhitelist.Whitelist, updatedWhitelistEntry] },
					});
				}
			});
			enqueueSnackbar('Whitelist entry successfully updated!', { variant: 'success' });
			setUpdateLoad(false);
		} catch (e) {
			enqueueSnackbar('Whitelist entry update failed', { variant: 'error' });
			setUpdateLoad(false);
		}
	}

	return (
		<React.Fragment>
			<WhitelistEditPage
				dataLoading={loading}
				loading={updateLoad}
				whitelistEntry={state.whitelistEntry}
				whitelistEntryErrors={state.whitelistEntryErrors}
				updateWhitelistEntry={updateWhitelistEntry}
				handleChange={handleChange}
				handleToggle={handleToggle}
			/>
		</React.Fragment>
	);
}