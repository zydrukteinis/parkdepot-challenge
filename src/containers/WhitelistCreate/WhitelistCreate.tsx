import React, { useReducer, useState } from 'react';
import moment from 'moment';
import { useAuth0 } from '@auth0/auth0-react';
import { useMutation } from '@apollo/client';
import { ADD_CAR_TO_WHITELIST, GET_WHITELIST } from '../../graphql/Queries';
import { WhitelistEntry, WhitelistEntryErrors } from '../../types/WhitelistEntry';
import { isWhitelistEntryPropValid } from '../../helpers/validation';
import WhitelistCreatePage from '../../components/WhiteListCreate/WhiteListCreatePage';
import { useSnackbar } from 'notistack';

type State = {
	whitelistEntry: WhitelistEntry | null;
	whitelistEntryErrors: WhitelistEntryErrors;
};

type Action =
	| { type: 'EDIT_WHITELIST_ENTRY', payload: { prop: string, value: any, } }
	| { type: 'SET_WHITELIST_PROP_ERROR', payload: { errorsProp: string, value: boolean, } }
	| { type: 'RESET_WHITELIST_ENTRY', payload: {} };


const generateWhitelistEntry = (): WhitelistEntry => {
	return {
		licensePlate: '',
		validFrom: moment().format('YYYY-MM-DDTHH:mm'),
		validTo: moment().add(1, 'hours').format('YYYY-MM-DDTHH:mm'),
		infiniteValidity: true,
	};
};

const initialState: State | any = {
	whitelistEntry: generateWhitelistEntry(),
	whitelistEntryErrors: {
		licensePlate: false,
		validFrom: false,
		validTo: false,
	},
};

const reducer = (state = initialState, action: Action) => {
    switch (action.type) {
        case 'EDIT_WHITELIST_ENTRY':
			return {
				...state,
				whitelistEntry: {
					...state.whitelistEntry,
					[action.payload.prop]: action.payload.value
				},
			};
		case 'SET_WHITELIST_PROP_ERROR':
			return {
				...state,
				whitelistEntryErrors: {
					...state.whitelistEntryErrors,
					[action.payload.errorsProp]: action.payload.value
				},
				}
		case 'RESET_WHITELIST_ENTRY':
			return {
				...state,
				whitelistEntry: generateWhitelistEntry(),
				whitelistEntryErrors: {
					licensePlate: false,
					validFrom: false,
					validTo: false,
				},
			}
		default:
			return state;
	};
};

export default function WhitelistCreate() {
	const [ loading, setLoading ] = useState(false);
	const { user } = useAuth0();
	const [ state, dispatch ] = useReducer(reducer, initialState);
	const { enqueueSnackbar } = useSnackbar();

	const validatePropChange = (prop: string, value: any) => {
		let isValid = isWhitelistEntryPropValid(prop, value, state.whitelistEntry);

		if (state.whitelistEntryErrors[prop] === !isValid) {
			return;
		}

		if (['validFrom', 'validTo'].includes(prop)) {
			dispatch({
				type: 'SET_WHITELIST_PROP_ERROR',
				payload: {
					errorsProp: 'validFrom',
					value: !isValid,
				}
			});
			dispatch({
				type: 'SET_WHITELIST_PROP_ERROR',
				payload: {
					errorsProp: 'validTo',
					value: !isValid,
				}
			});
			return;
		}

		dispatch({
			type: 'SET_WHITELIST_PROP_ERROR',
			payload: {
				errorsProp: prop,
				value: !isValid,
			}
		});
	};

	const handleChange = (event: React.ChangeEvent<{ name: string, value: any, valueAsNumber?: number, }>) => {
		const { name, value } = event.target;

		dispatch({
			type: 'EDIT_WHITELIST_ENTRY',
			payload: {
				prop: name,
				value,
			}
		});

		validatePropChange(name, value);
	};

	const handleToggle = (event: React.ChangeEvent<HTMLInputElement>): void => {
		const { name, checked } = event.target;

		dispatch({
			type: 'EDIT_WHITELIST_ENTRY',
			payload: {
				prop: name,
				value: checked,
			},
		});
	};

	const addToWhitelist = () => {
		setLoading(true);

		const whiteListEntryErrors = Object.keys(state.whitelistEntryErrors).map((propKey) => {
			let isValid = isWhitelistEntryPropValid(
				propKey,
				state.whitelistEntry[propKey],
				state.whitelistEntry
			);

			if (state.whitelistEntryErrors[propKey] === !isValid) {
				return !isValid;
			}

			dispatch({
				type: 'SET_WHITELIST_PROP_ERROR',
				payload: {
					errorsProp: propKey,
					value: !isValid,
				}
			});

			return !isValid;
		});

		if (whiteListEntryErrors.some(isError => isError)) {
			enqueueSnackbar('Invalid Fields', { variant: 'error' });
			setLoading(false);
			return;
		}

		try {
			whiteListInsertMutation({
				variables: {
					...state.whitelistEntry,
					createdBy: user.email,
				},
				optimisticResponse: true,
				update: (cache, { data }) => {
					const currentWhitelist: { Whitelist: WhitelistEntry[] } | null = cache.readQuery({
						query: GET_WHITELIST,
					});

					if (!currentWhitelist) {
						return;
					}

					const newWhitelistEntry = data.insert_Whitelist_one;

					cache.writeQuery({
						query: GET_WHITELIST,
						data: { Whitelist: [...currentWhitelist.Whitelist, newWhitelistEntry] },
					});
				}
			});
			enqueueSnackbar('Whitelist entry successfully created!', { variant: 'success' });
			dispatch({
				type: 'RESET_WHITELIST_ENTRY',
				payload: {},
			});
			setLoading(false);
		} catch (e) {
			enqueueSnackbar('Whitelist entry creation failed', { variant: 'error' });
			setLoading(false);
		}
	};

	const [ whiteListInsertMutation ] = useMutation(ADD_CAR_TO_WHITELIST);

	return (
		<WhitelistCreatePage
			loading={loading}
			whitelistEntry={state.whitelistEntry}
			whitelistEntryErrors={state.whitelistEntryErrors}
			addToWhitelist={addToWhitelist}
			handleChange={handleChange}
			handleToggle={handleToggle}
		/>
	);
};