import React from 'react';
import { useAuth0 } from '@auth0/auth0-react';
import { useQuery, useMutation } from "@apollo/client";
import { WhitelistEntry } from '../../types/WhitelistEntry';
import { GET_WHITELIST, DELETE_WHITELIST_ENTRY } from '../../graphql/Queries';
import { formatWhitelistEntry } from '../../helpers/whitelist';
import { isWhitelistEntryNotExpired } from '../../helpers/validation';
import { getUserRole } from '../../helpers/role';
import WhiteListPage from '../../components/Whitelist/WhitelistPage';

type Props = {
	history: any;
};

export default function Whitelist({ history } : Props) {
	const { user } = useAuth0();
	const { loading, data } = useQuery(GET_WHITELIST);
	const [ deleteWhitelistEntryMutation ] = useMutation(DELETE_WHITELIST_ENTRY);

	const getWhitelist = () => {
		return data
			? data.Whitelist.filter(isWhitelistEntryNotExpired).map(formatWhitelistEntry)
			: [];
	};

	const deleteWhitelistEntry = (id: string | undefined): void => {
		deleteWhitelistEntryMutation({
			variables: { id },
			optimisticResponse: true,
			update: (cache) => {
				const whiteListQuery: { Whitelist: WhitelistEntry[] } | null = cache.readQuery({ query: GET_WHITELIST });

				if (!whiteListQuery) {
					return;
				}

				const whiteList = whiteListQuery.Whitelist.filter((t) => t.id !== id);

				cache.writeQuery({
					query: GET_WHITELIST,
					data: { Whitelist: whiteList },
				});

			},
		});
	};

	const openWhitelistEntryEdit = (rowData: any) => {
		history.replace('')
		history.push(`/whitelist/${rowData.id}/edit`)
	};

	return (
		<React.Fragment>
			<WhiteListPage
				userEmail={user && user.email}
				userRole={getUserRole(user)}
				loading={loading}
				whitelist={getWhitelist()}
				deleteWhitelistEntry={deleteWhitelistEntry}
				openWhitelistEntryEdit={openWhitelistEntryEdit}
			/>
		</React.Fragment>
	);
};