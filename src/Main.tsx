import React from 'react';
import { Switch, Route } from 'react-router-dom'
import { useAuth0 } from "@auth0/auth0-react";
import WhitelistCreate from './containers/WhitelistCreate/WhitelistCreate';
import { makeStyles } from '@material-ui/core/styles';
import CircularProgress from '@material-ui/core/CircularProgress';
import Backdrop from '@material-ui/core/Backdrop';
import IndexPage from './components/IndexPage/IndexPage';
import Whitelist from './containers/Whitelist/Whitelist';
import WhitelistEdit from './containers/WhitelistEdit/WhitelistEdit';
import { getUserRole } from './helpers/role';

type Props = {
	history: any,
};

const useStyles = makeStyles((theme) => ({
	root: {
		display: 'flex',
		justifyContent: 'center',
		marginTop: theme.spacing(10),
	},
	backdrop: {
		zIndex: theme.zIndex.drawer + 1,
		color: '#fff',
	},
}));

export default function Main({ history } : Props) {
	const classes = useStyles();
	const { user, isLoading, isAuthenticated, loginWithRedirect } = useAuth0();

	return (
		<React.Fragment>
			<Backdrop className={classes.backdrop} open={isLoading}>
				<div className={classes.root}>
					<CircularProgress size={50}/>
				</div>
			</Backdrop>
			<Switch>
				{ !isAuthenticated &&
					<Route
						path='/'
						render={() => <IndexPage
							history={history}
							isLoading={isLoading}
							isAuthenticated={isAuthenticated}
							loginWithRedirect={loginWithRedirect}
						/>}
					/>
				}
				<Route
					exact path='/'
					render={() => <IndexPage
						history={history}
						isLoading={isLoading}
						isAuthenticated={isAuthenticated}
						loginWithRedirect={loginWithRedirect}
					/>}
				/>
				{ isAuthenticated &&
					<>
						{
							getUserRole(user) && <Route
								exact path='/whitelist/add'
								render={() => <WhitelistCreate/>}
							/>
						}
						<Route
							exact path='/whitelist/:id/edit'
							render={(routerProps) =>
								<WhitelistEdit
									id={routerProps.match.params.id}
								/>
							}
						/>
						<Route
							exact path='/whitelist'
							render={() => <Whitelist history={history}/>}
						/>
					</>
				}

			</Switch>
		</React.Fragment>
	);
}