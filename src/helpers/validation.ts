import { WhitelistEntry } from '../types/WhitelistEntry';
import moment from 'moment';

export const isWhitelistEntryPropValid = (
	prop: string,
	value: any,
	whitelistEntry: WhitelistEntry
) : boolean  => {
	let isValid = true;

	switch(prop) {
		case 'validFrom':
			isValid = whitelistEntry.infiniteValidity || moment(value) < moment(whitelistEntry.validTo);
			break;
		case 'validTo':
			isValid = whitelistEntry.infiniteValidity || (
				moment() < moment(value)
				&& moment(whitelistEntry.validFrom) < moment(value)
			);
			break;
		case 'licensePlate':
		default:
			isValid = value !== '';
			break;
	};

	return isValid;
}

export const isWhitelistEntryNotExpired = (whitelistEntry: WhitelistEntry) => {
	return whitelistEntry.infiniteValidity || (
		moment() < moment(whitelistEntry.validTo)
	);
};