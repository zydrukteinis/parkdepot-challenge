export const getUserRole = (user: any) => {
	const roles = user[`${process.env.REACT_APP_AUTH0_NAMESPACE}/roles`];
	const userRole = roles && roles[0];

	return userRole;
};