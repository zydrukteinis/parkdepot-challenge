import moment from 'moment';
import { WhitelistEntry } from '../types/WhitelistEntry';

const getDateTime = (fieldKey: string, whitelistEntry: WhitelistEntry | any) => {
	if (whitelistEntry.infiniteValidity) {
		return '-';
	}

	if (!whitelistEntry[fieldKey]) {
		return;
	}

	return moment(whitelistEntry[fieldKey]).format('YYYY-MM-DD HH:mm');
}

export const formatWhitelistEntry = (whitelistEntry: WhitelistEntry)  => ({
	id: whitelistEntry.id,
	createdBy: whitelistEntry.createdBy,
	licensePlate: whitelistEntry.licensePlate.toUpperCase(),
	validFrom: getDateTime('validFrom', whitelistEntry),
	validTo: getDateTime('validTo', whitelistEntry),
	infiniteValidity: whitelistEntry.infiniteValidity,
});