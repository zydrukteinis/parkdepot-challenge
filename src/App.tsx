import './styles/main.scss';
import React, { useEffect, useState } from 'react';
import { ApolloClient, ApolloProvider, InMemoryCache } from "@apollo/client";
import { useAuth0 } from "@auth0/auth0-react";
import { createBrowserHistory } from "history";
import { Router } from 'react-router-dom'
import { makeStyles } from '@material-ui/core/styles';
import Navigation from './components/Navigation/Navigation';
import Container from '@material-ui/core/Container';
import { SnackbarProvider } from 'notistack';
import Main from './Main';

const history = createBrowserHistory();

const useStyles = makeStyles((theme) => ({
  root: {
    display: 'flex',
  },
  container: {
    paddingTop: theme.spacing(12),
    paddingBottom: theme.spacing(8),
  },
  content: {
    flexGrow: 1,
    padding: theme.spacing(3),
  },
}));

function App() {
  const classes = useStyles();
  const [ accessToken, setAccessToken ] = useState('');
  const { isAuthenticated, getAccessTokenSilently } = useAuth0();

  useEffect(() => {
    const getAccessToken = async () => {
      try {
        const token = await getAccessTokenSilently();
        setAccessToken(token);
      } catch (e) {
        console.error(e);
      }
    };

    if (!isAuthenticated) {
      return;
    }
    getAccessToken();

  // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [isAuthenticated]);

  const apolloClient = new ApolloClient({
    uri: process.env.REACT_APP_HASURA_GRAPHQL_URL,
    headers: {
      authorization: `Bearer ${accessToken}`,
      'content-type': 'application/json',
      'x-hasura-admin-secret': process.env.REACT_APP_HASURA_GRAPHQL_ADMIN_SECRET || ''
    },
    cache: new InMemoryCache(),
  });

  return (
    <div className="App">
      <div className={classes.root}>
        <ApolloProvider client={apolloClient}>
          <SnackbarProvider maxSnack={5}
            anchorOrigin={{
              vertical: 'top',
              horizontal: 'right',
            }}
          >
            <Router history={history}>
              <Navigation/>
              <Container className={classes.container} maxWidth="xl">
                <Main history={history}/>
              </Container>
            </Router>
          </SnackbarProvider>
        </ApolloProvider>
      </div>
    </div>
  );
}

export default App;
