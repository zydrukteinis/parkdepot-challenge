## Parkdepot Frontend Challenge

This is a demo project for `Parkdepot Frontend Challenge` 🚀

### Architecture
For architecture - container pattern was used.

### Authentication
For user authentication & roles management `auth0` was used.

#### Roles
- Manager - allowed to view whitelist, create, edit & delete all whitelist entries;
- Employee - allowed to view whitelist, create, edit & delete whitelist entries that were created by himself;
- Guest - only allowed to view whitelist;

### Demo Users
- Manager - email: `parkdepot-demo-manager@outlook.com`, password: `manager`;
- Employee - email: `parkdepot-demo-employee@outlook.com`, password: `employee`;
- Guest - email: `parkdepot-demo-guest@outlook.com`, password: `guest`;

You can also create your users through email sign-up or Google/Facebook authentication.

### Database & GraphQL
For database - Hasura.io cloud postgre DB was created, which can be queried, mutated through Apollo client.

### Demo
To launch the demo:
- Install the dependencies - `npm run install`;
- Run the demo via - `npm run dev`;

### Note
For the sake of demo simplicity, local configuration file `.env` in the root directory was included.
In a real-world application repository, `.env.*` files must be listed in the `.gitignore`.